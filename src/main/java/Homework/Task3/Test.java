package Homework.Task3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Test {
    public static void main(String[] args) {
        Student student1 = new Student();
        student1.setName("Ivan");
        student1.setCity("Moscow");
        student1.setGroup(1);
        student1.setYear(1000);
        Student student2 = new Student();
        student2.setName("Andrey");
        student2.setCity("Moscow");
        student2.setGroup(5);
        student2.setYear(1995);
        Student student3 = new Student();
        student3.setName("Airat");
        student3.setCity("Kazan");
        student3.setGroup(3);
        student3.setYear(2005);
        ArrayList<Student> students = new ArrayList<>();
        students.add(student1);
        students.add(student2);
        students.add(student3);
        try (FileWriter writer = new FileWriter("C:\\Users\\admin\\IdeaProjects\\Nizamov_11805\\src\\main\\java\\Homework\\Task3\\result", false)) {
            for (Student student :
                    students) {
                if ((student.getYear() < 1999) && (student.getCity().equals("Moscow"))) {
                    writer.write(String.valueOf(student));
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}