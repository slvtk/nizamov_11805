package Homework.Task3;

public class Student {
    private String name;
    private int group;
    private String city;
    private int year;

    @Override
    public String toString() {
        return "Student " +
                "name=" + name +
                ", group=" + group +
                ", city='" + city + '\'' +
                ", year=" + year+"\n"
                ;
    }

    int getGroup() {
        return group;
    }

    void setGroup(int group) {
        this.group = group;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    String getCity() {
        return city;
    }

    void setCity(String city) {
        this.city = city;
    }

    int getYear() {
        return year;
    }

    void setYear(int year) {
        this.year = year;
    }
}
