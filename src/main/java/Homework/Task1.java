package Homework;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        double a = 1, b = 1, c = 0;
        for (int k = 1; k <= n; k++) {
            a *= 2;
            b *= k;
            c += a / b;

        }
        System.out.println(c);
    }
}