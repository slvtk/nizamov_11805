package Homework.Task2;

public class Main {
    public static void main(String[] args) {
        User user=new User();
        Admin adminUser=new Admin();
        PremiumUser premiumUser=new PremiumUser();
        Count count=new Count();
        user.setAge(18);
        user.setName("Ivan");
        premiumUser.setCity("Kazan");
        premiumUser.say();
        adminUser.changeAge(user,15);
        adminUser.changeName(user,"Anton");
        count.countPass(user);
        System.out.println(user);
    }
}
