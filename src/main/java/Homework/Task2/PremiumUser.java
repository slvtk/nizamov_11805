package Homework.Task2;

public class PremiumUser extends User {

    private String city;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void say(){
        System.out.println("Hello, I live in "+this.city);
    }
}