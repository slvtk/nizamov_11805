package Homework.TaskSeptember23;

import java.util.HashMap;
import java.util.Map;

class Dispatcher {
    void dispatch(String id) throws Exception {
        User user = new User();
        Map<String, String> mapParams = new HashMap<>();
        String[] idParts = id.split("[?]");
        String path = idParts[0];//переменная содержащая путь
        String[] params = idParts[1].split("[&]|[=]");//массив из разделенных параметров

        //System.out.println(path); путь до знака вопроса
        for (int k = 0; k <= params.length - 2; k += 2) {
            mapParams.put(params[k], params[k + 1]);
        }
        //System.out.println(mapParams);

        switch (path) {
            case ("profile"):
                ProfileService profileService = new ProfileService();
                profileService.findInfo(mapParams, user);
                break;
            case ("friends"):
                FriendsService friendsService = new FriendsService();
                friendsService.findInfo(mapParams, user);
                break;
            case ("im"):
                ImService imService = new ImService();
                imService.findInfo(mapParams, user);
                break;
            case ("om"):
                OmService omService = new OmService();
                omService.findInfo(mapParams, user);
                break;
            case ("messages"):
                MessagesService messagesService = new MessagesService();
                messagesService.findInfo(mapParams, user);
                break;
        }
    }
}
