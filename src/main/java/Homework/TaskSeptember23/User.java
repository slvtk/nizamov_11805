package Homework.TaskSeptember23;

import java.util.ArrayList;

public class User {

    private String id;
    private String name;
    private String surname;
    private String city;
    private ArrayList<String> friends = new ArrayList<>();
    private ArrayList<String> iMessages = new ArrayList<>();
    private ArrayList<String> oMessages = new ArrayList<>();

    String getId() {
        return id;
    }

    void setId(String id) {
        this.id = id;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    String getSurname() {
        return surname;
    }

    void setSurname(String surname) {
        this.surname = surname;
    }

    String getCity() {
        return city;
    }

    void setCity(String city) {
        this.city = city;
    }

    ArrayList<String> getFriends() {
        return friends;
    }

    void setFriends(String friend) {
        this.friends.add(friend);
    }

    ArrayList<String> getiMessages() {
        return iMessages;
    }

    void setiMessages(String im) {
        this.iMessages.add(im);
    }

    ArrayList<String> getoMessages() {
        return oMessages;
    }

    void setoMessages(String om) {
        this.oMessages.add(om);
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
