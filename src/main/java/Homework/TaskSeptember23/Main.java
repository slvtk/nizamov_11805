package Homework.TaskSeptember23;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        Scanner scan = new Scanner(System.in);
        String url = scan.nextLine();
        Dispatcher dispatcher = new Dispatcher();
        dispatcher.dispatch(url);
    }
}
