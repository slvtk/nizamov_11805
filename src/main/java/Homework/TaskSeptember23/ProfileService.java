package Homework.TaskSeptember23;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.Map;
import java.util.Scanner;

class ProfileService {

    void findInfo(Map<String, String> mapParams, User user) throws Exception {
        String id = mapParams.get("id");
        FileReader fr = new FileReader("C:\\Users\\admin\\IdeaProjects\\Nizamov_11805\\src\\main\\java\\Homework\\TaskSeptember23\\users");
        FileWriter fw = new FileWriter("C:\\Users\\admin\\IdeaProjects\\Nizamov_11805\\src\\main\\java\\Homework\\TaskSeptember23\\index");
        Scanner scan = new Scanner(fr);
        String[] fields;
        while (scan.hasNextLine()) {
            String currentLine = scan.nextLine();
            if (currentLine.contains(id)) {
                fields = currentLine.split(",");
                user.setId(id);
                user.setName(fields[1]);
                user.setSurname(fields[2]);
                user.setCity(fields[3]);
            }
        }
        if (mapParams.get("friends") == null) {
            fw.write(
                    "<!doctype html>\n" +
                            "<html>\n" +
                            "<head>\n" + "<title>Page</title>\n" +
                            "</head>\n" +
                            "<body>\n<p>ID: " +
                            user.getId() +
                            "</p>\n<p>Name: " +
                            user.getName() +
                            "</p>\n<p>Surname: " + user.getSurname() +
                            "</p>\n<p>City: " + user.getCity() +
                            "</p>\n" + "</body>\n" +
                            "</html>");
            fr.close();
            fw.close();
        } else {
            FriendsService friendsService = new FriendsService();
            friendsService.findInfo(mapParams, user);
            fw.write(
                    "<!doctype html>\n" +
                            "<html>\n" +
                            "<head>\n" +
                            "<title>Page</title>\n" +
                            "</head>\n" +
                            "<body>\n<p>ID: " + user.getId() +
                            "</p>\n<p>Name: " + user.getName() +
                            "</p>\n<p>Surname: " + user.getSurname() +
                            "</p>\n<p>City: " + user.getCity() +
                            "</p>\n<p>Friends: " + user.getFriends() +
                            "</p>\n" + "</body>\n" +
                            "</html>");
            fw.close();
        }
    }
}
