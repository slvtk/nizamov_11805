package Homework.TaskSeptember23;

import java.util.Map;

class MessagesService {
    void findInfo(Map mapParams, User user) throws Exception {
        String id1 = mapParams.get("id1").toString();
        String id2 = mapParams.get("id2").toString();
        user.setId(id1);
        ImService imService=new ImService();
        imService.findInfo(mapParams,user);
        user.setId(id2);
        OmService omService=new OmService();
        omService.findInfo(mapParams,user);
    }
}
