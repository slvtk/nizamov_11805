package Classwork;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Client {

    public static void main(String[] args) throws IOException {
        final String HOST = "localhost";
        final int PORT = 1234;
        System.out.println("Connecting to " + HOST + ":" + PORT + ".");
        Socket s = new Socket(HOST, PORT);
        System.out.println("Connected");
        OutputStream outputStream = s.getOutputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(s.getInputStream()));
        Scanner scan = new Scanner(System.in);
        PrintWriter writer = new PrintWriter(outputStream, true);

        while (true) {
            String string = reader.readLine();
            System.out.println(string);
            writer.println(scan.next());
        }
    }
}
