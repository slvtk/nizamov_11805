package Classwork;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {

    public static void main(String[] args) throws IOException {

        final int PORT = 1234;
        System.out.println("Server started");
        ServerSocket ss = new ServerSocket(PORT);
        System.out.println("Opening connection on port " + PORT);
        Socket s = ss.accept();
        System.out.println("Connected");
        InputStream inputStream = s.getInputStream();
        Scanner scan = new Scanner(System.in);
        OutputStream outputStream = s.getOutputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        PrintWriter writer = new PrintWriter(outputStream, true);

        while (true) {
            writer.println(scan.next());
            String string = reader.readLine();
            System.out.println(string);
        }
    }
}
